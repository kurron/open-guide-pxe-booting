# Overview
This document describes how I've configured my Raspberry Pi to allow for machines to boot
[CoreOS](https://coreos.com/os/docs/latest) over the network. As an unexpected benefit, machines
can also boot up [SpinRite](https://www.grc.com/sr/spinrite.htm) so you don't have to hunt
for USB sticks or CD-ROMs.

# Prerequisites
* Rasbperry Pi Model B or greater
* [Raspbian Linux](https://www.raspberrypi.org/downloads/raspbian/) installed and working

# Building
There is no software to build.

# Installation
1. `ssh-keygen -C CoreOS -f coreos` -- CoreOS uses keyed SSH for remote access so we'll need a new pair
1. ssh into the Raspberry Pi
1. `sudo apt-get update` -- just making sure we have current packages
1. `sudo apt-get install pxelinux dnsmasq syslinux-common wget` -- install the packages we need
1. `sudo mkdir -p /var/lib/tftpboot` -- this will be the home for all of the PXE related files
1. `sudo cp /usr/lib/PXELINUX/pxelinux.0 /var/lib/tftpboot/` -- copy the critical pxelinux.0 file
1. `sudo cp /usr/lib/syslinux/modules/bios/* /var/lib/tftpboot/` -- copy support files
1. `sudo cp -r /usr/lib/syslinux/* /var/lib/tftpboot/` -- copy files needed to boostrap the booting process
1. `sudo mkdir /var/lib/tftpboot/pxelinux.cfg` -- this holds the PXE configuration inlcluding the menu shown to the user
1. `sudo vi /var/lib/tftpboot/pxelinux.cfg/default` -- create this file using the contents shown below
1. `sudo vi /etc/dnsmasq.d/pxe.cfg` -- create this file using the contents shown below. **Must match your network.**
1. `sudo service dnsmasq restart` -- restart the DHCP proxy
1. `coreos.ign` must be available via HTTP because CoreOS will process it at boot.  The location should match what is
in `/var/lib/tftpboot/pxelinux.cfg/default`
1. `sudo mkdir -p /var/lib/tftpboot/coreos/stable` -- create a folder to hold the CoreOS boot images
1. `cd /var/lib/tftpboot/coreos/stable` -- we'll be copying the images to here
1. `sudo wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_pxe.vmlinuz`
1. `sudo wget https://stable.release.core-os.net/amd64-usr/current/coreos_production_pxe_image.cpio.gz`
1. `sudo mkdir -p /var/lib/tftpboot/spinrite` -- create a folder to hold the SpinRite ISO
1. copy the SpinRite ISO to `/var/lib/tftpboot/spinrite/cdrom.img` -- you need to have purchased a copy of SpinRite
1. configure your machine's BIOS so it boots from the network and test it out
1. connect to your machine using the keys you generated, eg. `ssh -i id_coreos core@192.168.1.240`
 

## /var/lib/tftpboot/pxelinux.cfg/default
```
DEFAULT menu.c32
PROMPT 0
TIMEOUT 300
ONERROR LOCALBOOT 0
ONTIMEOUT local

MENU TITLE PXE Boot

LABEL local
  MENU LABEL From Hard Drive
  LOCALBOOT 0

LABEL CoreOS
  MENU LABEL CoreOS (stable)
  KERNEL coreos/stable/coreos_production_pxe.vmlinuz
  INITRD coreos/stable/coreos_production_pxe_image.cpio.gz
  APPEND coreos.first_boot=1 coreos.config.url=https://raw.githubusercontent.com/kurron/scripts/master/tftp/coreos.ign

LABEL SpinRite
  MENU LABEL SpinRite
  KERNEL memdisk
  INITRD spinrite/cdrom.img
  APPEND iso
```

## /etc/dnsmasq.d/pxe.cfg
```
interface=eth0
dhcp-range=192.168.1.0,proxy
dhcp-boot=pxelinux.0
pxe-service=x86PC,"Booting from Network...",pxelinux
enable-tftp
tftp-root=/var/lib/tftpboot
dhcp-boot=pxelinux.0,raspberrypi,192.168.1.236
```
 
## coreos.ign
```
{
  "ignition": {
    "version": "2.0.0",
    "config": {}
  },
  "storage": {},
  "systemd": {
    "units": [
      {
        "name": "etcd2.service",
        "enable": true
      }
    ]
  },
  "networkd": {},
  "passwd": {
    "users": [
      {
        "name": "core",
        "sshAuthorizedKeys": [
          "ssh-rsa AAAAB3NzaC1y... rkurr@batcave"
        ]
      }
    ]
  }
}
```

# Tips and Tricks

## Refresh Your Hard Drives
[SpinRite](https://www.grc.com/sr/spinrite.htm) is a recovery tool but I've used it mostly as a
preventative measure.  I tend to misplace thumb drives which made it more difficult to be disciplined and
refresh my drives as often as I should.  Once I figured out that I could quickly boot SpinRite over the
network, I didn't think twice about running scans.  I also noticed that some of my hardware that had issues
booting SpinRite from USB had no issues booting over the network.  SpinRite is tiny so it boots almost
instantly.  I highly recommend you purchase a copy and avoid losing data.

## VirtualBox Machines
CoreOS is meant to run in a cluster and I've found that you can boot VirtualBox machines from the network
like you can with bare metal.  This makes it very convenient to test out new configurations.  PXE booted
CoreOS does not need a hard drive because everything runs in RAM which means you don't have to constantly
create/destroy machines.  Just reboot the same VM and select a different image or configuration to run and
you are good to go.

# Troubleshooting

## Networking
PXE booting only works over ethernet so you have to be hard wired to a LAN.  Also, the `pxe.cfg` file's network
settings must match your internal network.  I'm using the `192.168.1.0` network so that it is what is in the
example.  If you are using `10.0.0.0` or `172.16.0.0`, you will have to adjust the configuration accordingly.

## Network Cards
Go into your BIOS and ensure that your machine has network booting enabled.  It has been my experience that
new machines have that option normally disabled.  None of this will work if your NIC doesn't search the network
for boot instructions.

## Booting RacherOS
Another Docker-only operating system is [RancherOS](http://rancher.com/rancher-os/) but their documentation
talks about booting via iPXE.  I was able to convert they instructions to something that works with PXE.
In short, you have to download the bits instead of having iPXE pull them down dynamically.  You also have to
provide a cloud-config file to hold your SSH keys.

1. `sudo mkdir -p /var/lib/tftpboot/rancheros/latest/`
1. `cd /var/lib/tftpboot/rancheros/latest/`
1. `sudo vi update-image.sh` -- see contents below
1. `./update-image.sh` -- pulls down the current bits
1. `sudo vi /var/lib/tftpboot/pxelinux.cfg/default` -- add in the lines shown below
1. create a `rancher-os.config` file -- use the example below as a template
1. make your `rancher-os.config` available via HTTP and ensure the URL matches what is in `default`

### update-image.sh

```
#!/bin/bash

sudo wget http://releases.rancher.com/os/latest/vmlinuz
sudo wget http://releases.rancher.com/os/latest/initrd
```

### Updates to default

```
LABEL Rancher
  MENU LABEL RancherOS (latest)
  KERNEL rancheros/latest/vmlinuz
  INITRD rancheros/latest/initrd
  APPEND rancher.cloud_init.datasources=[url:https://raw.githubusercontent.com/kurron/scripts/master/tftp/rancher-os.config]
```

### rancher-os.config

```
#cloud-config
ssh_authorized_keys:
  - ssh-rsa AAAAB3Nza...
```

# License and Credits
This project is licensed under the [Apache License Version 2.0, January 2004](http://www.apache.org/licenses/).

* [Linux PXE Server](http://it-joe.com/linux/pxe_server) -- this inspired me to try all of this in the first place
